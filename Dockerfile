FROM python:3.10.10 as build

WORKDIR /app

COPY . /app/

WORKDIR /app/flask-server

RUN pip install -r requirements.txt

EXPOSE 8080
CMD ["python", "server.py"]
