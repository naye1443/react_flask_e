const ImageModal = ({ isOpen, imageUrl, onClose }) => {
    if (!isOpen) {
        return null;
    }

    return (
        <div
            style={{
                position: 'fixed',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                backgroundColor: 'rgba(0, 0, 0, 0.8)',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: 1000,
            }}
            onClick={onClose}
        >
            <img src={imageUrl} alt="Popup" style={{ maxWidth: '90%', maxHeight: '90%' }} />
        </div>
    );
};

export default ImageModal;