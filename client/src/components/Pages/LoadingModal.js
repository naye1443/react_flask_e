import '../../Style/LoadingModal.css'

const LoadingModal = ({isOpen}) => {
    return (
        <div className={`loading-modal ${isOpen ? 'visible' : 'hidden'}`}>
            <div className="loading-message">Loading...</div>
        </div>
    );
};

export default LoadingModal;